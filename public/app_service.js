let FAClient = null;
let notyf = null;
let apiKey = null;
let bingKey = null;
let recordsGlobal = false;
let routeResultGlobal = null;

let selectedWaypoints = [];
let selectedAccounts = [];


let locations = {
    "3a0fafb2-09ec-43d8-916e-52f676206b65":{
        "location":"30.02001479999999, -90.1714326"
    },
    "6be11dba-a322-449d-93ae-8add0ff3654c":{
        "location":"37.8971443, -122.0582731"
    },
    "88b3fcce-e257-4a78-9073-31d169bcc641":{
        "location":"37.74478939999999, -122.4523297"
    },
    "817a2e9e-0991-4717-ac4c-a7edc15dfc9b":{
        "location":"37.7064875, -122.4188589"
    },
    "02600631-eec3-434f-b982-e1e06fb076d8":{
        "location":"29.17157539999999, -81.0373353"
    },
    "a3089336-236f-48ea-bec5-455e3d897f64":{
        "location":"44.5306808, -89.6063278"
    },
    "c813c60c-20c0-4bfa-9888-03efb50727e1":{
        "location":"37.7609486, -122.4895545"
    },
    "deaf456b-988a-4384-9a06-28650148e78b":{
        "location":"36.778261, -119.4179324"
    },
    "43e0f833-11d8-4272-8862-a3d3735525e1":{
        "location":"37.7814126, -122.4606499"
    },
    "95e21b65-f440-47ce-9ede-98d84774deb3":{
        "location":"37.8905881, -122.1260935"
    },
    "73462c11-b5ab-4a47-9665-58f56fb50f64":{
        "location":"37.7297604, -122.4662731"
    },
    "b0cd1d88-9efb-4436-aa89-7fc377603fd7":{
        "location":"37.8698603, -122.2879176"
    },
    "107a3842-10ef-430b-8380-f3f428bb5769":{
        "location":"37.9035848, -122.5444334"
    },
    "957f961e-f1b8-4394-a613-7facb6e94e09":{
        "location":"37.8496917, -122.4809105"
    },
    "fe639524-2834-421b-9e8d-a74a70e35032":{
        "location":"37.7396287, -122.4190081"
    },
    "fcb74dc1-ea58-4805-8d07-53bf8b373487":{
        "location":"37.8235281, -122.0014918"
    },
    "16e2ef6c-ec6d-4f1c-82b5-99061cf9eeac":{
        "location":"37.79743130000001, -122.4218249"
    },
    "6b8ba50b-ceaa-4218-b81c-d8d04af00c17":{
        "location":"37.89133640000001, -122.5174773"
    },
    "553f70ac-4d9a-40e6-ab0c-c6920c0abbe1":{
        "location":"37.9236363, -122.0757994"
    },
    "7d9ce6d3-03eb-45cf-88de-8655224d4c08":{
        "location":"38.1059522, -122.2789335"
    },
    "62a48ddb-666c-4cc4-8463-bc720fd44104":{
        "location":"37.8915277, -122.0780188"
    }
}

let config = {
    appName: "logo",
    locationField: "hq_location",
    regionField: "logo_field49",
    directionsUrlFiled: "task_field15",
    accountsToVisitField: "task_field16",
    nameField: "name",
    taskType: "9bbeb547-82ca-48fa-aa5e-13baa77b2b23",
    center: { lat: 37.8698603, lng: -122.0582731 },
    startEndLocation: "37.8698603, -122.0582731",
    directionsBaseUrl: "https://www.google.com/maps/dir/?api=1"
}

let center = { lat: 37.8698603, lng: -122.0582731 };

let startEndLocation = "37.8698603, -122.2611597";


// aHR0cDovL2xvY2FsaG9zdDo1MDAw
// aHR0cHM6Ly9mYS1zb2x1dGlvbnMuZ2l0bGFiLmlvL21hcHMtanMtcXMtZGVtby1hcHBsZXQ=   // https://fa-solutions.gitlab.io/maps-js-qs-demo-applet

const SERVICE = {
    name: 'FreeAgentService',
    appletId: `aHR0cHM6Ly9mYS1zb2x1dGlvbnMuZ2l0bGFiLmlvL21hcHMtanMtcXMtZGVtby1hcHBsZXQ=`,
};



function startupService() {
    notyf = new Notyf({
        duration: 20000,
        dismissible: true,
        position: {
            x: 'center',
            y: 'bottom',
        },
        types: [
            {
                type: 'info',
                className: 'info-notyf',
                icon: false,
            },
        ],
    });

    FAClient = new FAAppletClient({
        appletId: SERVICE.appletId,
    });

    apiKey = FAClient.params.key;
    bingKey = FAClient.params.bingKey;


    FAClient.on('displayLocations', ({records}) => {
        recordsGlobal = records;
        FAClient.open();
        const map = new google.maps.Map(document.getElementById("map"), {
            zoom: 10,
            center: config.center,
        });

        const geocoder = new google.maps.Geocoder();

        records?.map(({ field_values, id }) => {
            let address = field_values[config.locationField]?.value;
            let name = field_values[config.nameField]?.value;
            setMarker({
                address: address,
                geocoder,
                map,
                icon: null,
                title: `${id}|${name}`
            })
        })

         let getRouteButton = document.querySelector('#get-route-button');
        getRouteButton.addEventListener('click', (event) => {
            if(getRouteButton.textContent === 'Save Route') {
                createTask();
            } else {
                document.querySelector('#map').style.width = "63%";
                document.querySelector('#right-panel').style.display = "block";
                getRouteButton.textContent = 'Save Route';
                initMap(config.startEndLocation, config.startEndLocation);
            }
        })
        console.log({records})
    })

}



function initMap(startLocation, endLocation) {
    let markerArray = [];

    const map = new google.maps.Map(document.getElementById("map"), {
        zoom: 4,
        center, // Australia.
    });

    const stepDisplay = new google.maps.InfoWindow();


    const directionsService = new google.maps.DirectionsService();
    const directionsRenderer = new google.maps.DirectionsRenderer({
        draggable: true,
        map,
        panel: document.getElementById("right-panel"),
    });
    directionsRenderer.addListener("directions_changed", () => {
        console.log(directionsRenderer.getDirections())
        computeTotalDistance(directionsRenderer.getDirections());
    });
    displayRoute(
        startLocation,
        endLocation,
        selectedWaypoints,
        directionsService,
        directionsRenderer,
        stepDisplay,
        markerArray,
        map
    );

}

function createTask() {
    if(routeResultGlobal) {
        let { geocoded_waypoints, request, routes } = routeResultGlobal;
        let legs = routes[0].legs;
        let directionsUrl = `${config.directionsBaseUrl}&${getPlaceIdParams(geocoded_waypoints)}&${getParamsFromLegs(legs)}&dir_action=navigate`;
        let taskDescription = recordsGlobal[0]?.field_values[config.regionField] ? `Visiting Accounts in ${recordsGlobal[0]?.field_values[config.regionField]?.display_value}` : 'Visiting Accounts';
        let taskVars = {
            "entity": "task",
            "field_values": {
                "task_type": config.taskType,
                "task_status": false,
                "overdue": false,
                "send_automatically": false,
                "send_invitations": false,
                "assigned_to": "d666d18d-b5f8-4dd9-b447-31b1363b141d",
                "description": taskDescription,
                [config.directionsUrlFiled] : directionsUrl,
                [config.accountsToVisitField] : selectedAccounts
            }
        }

        FAClient.createEntity(taskVars, ({entity_value}) => {
            console.log(entity_value);
            FAClient.showModal('entityFormModal', {
                entity: "task",
                entityLabel: 'Accounts Visit Task',
                entityInstance: entity_value,
                showButtons: false,
            });
            //FAClient.navigateTo(`/task/view/${entity_value.id}`);
        })

    }
}

function displayRoute(origin, destination, waypoints, service, display, stepDisplay, markerArray, map) {
    service.route(
        {
            origin: origin,
            destination: destination,
            waypoints,
            travelMode: google.maps.TravelMode.DRIVING,
            avoidTolls: false,
            optimizeWaypoints: true
        },
        (result, status) => {
            if (status === "OK" && result) {
                routeResultGlobal = result;

                display.setDirections(result);
                let distance = document.querySelector('#distance-container');
                let duration = document.querySelector('#duration-container');
                if(distance && duration) {
                    distance.style.display = "flex";
                    duration.style.display = "flex";
                }
            } else {
                window.alert("Directions request failed due to " + status);
            }
        }
    );
}

function computeTotalDistance(result) {
    let total = 0;
    let totalTime = 0;
    const myroute = result.routes[0];

    if (!myroute) {
        return;
    }

    for (let i = 0; i < myroute.legs.length; i++) {
        total += myroute.legs[i].distance.value;
        totalTime += myroute.legs[i].duration.value;
        console.log(myroute.legs[i])
    }
    total = total / 1000;
    totalTime = totalTime/60;
    document.getElementById("total").innerHTML = (total / 1.609).toFixed(2) + " mi";
    document.getElementById("totalDuration").innerHTML = Math.floor(totalTime) + " min";
}




function setMarker({address,
                   geocoder,
                   map,
                   isCenter = false,
                   icon=null,
                   title=null,
                   color = "#ea4435"
           }) {

    let accountName = title?.split('|')[1];
    let accountId = title?.split('|')[0];



    if(!locations[accountId]) {
        geocoder.geocode({ address: address }, (results, status) => {
            if (status === "OK") {
                locations[accountId] = {location : `${results[0].geometry.location.lat()}, ${results[0].geometry.location.lng()}` }
                let position = results[0].geometry.location;
                addMarker(position, icon, map, isCenter, accountId, accountName)
            } else {
                alert("Geocode was not successful for the following reason: " + status);
            }
        });
    } else {
        addMarker(getLocation(locations[accountId].location), icon, map, isCenter, accountId, accountName)
    }
}


function addMarker(position, icon, map, isCenter, accountId, accountName, color = "#ea4435") {
    const infoWindow = new google.maps.InfoWindow();

    const svgMarker = {
        path: "M10.453 14.016l6.563-6.609-1.406-1.406-5.156 5.203-2.063-2.109-1.406 1.406zM12 2.016q2.906 0 4.945 2.039t2.039 4.945q0 1.453-0.727 3.328t-1.758 3.516-2.039 3.070-1.711 2.273l-0.75 0.797q-0.281-0.328-0.75-0.867t-1.688-2.156-2.133-3.141-1.664-3.445-0.75-3.375q0-2.906 2.039-4.945t4.945-2.039z",
        fillColor: color,
        fillOpacity: 1,
        strokeWeight: 0,
        rotation: 0,
        scale: 2,
        anchor: new google.maps.Point(12, 23),
    };

    if(accountName?.includes('VALE Aerospace Inc.')) {
        svgMarker.fillColor = "#00954e";
        icon = svgMarker;
        isCenter = true;
    }

    if(isCenter) {
        map.setCenter(position);
        if(!selectedAccounts.includes(accountId)) {
            selectedAccounts.push(accountId);
        }
    }
    const marker = new google.maps.Marker({
        map: map,
        position,
        icon,
        optimized: true,
    });
    marker.addListener("click", (event) => {
        if (event?.domEvent?.altKey) {
            FAClient.navigateTo(`/logo/view/${accountId}`)
        } else {
            marker.setIcon(svgMarker)
            if(!isCenter) {
                selectedWaypoints.push({ location: `${marker.getPosition().lat()},${marker.getPosition().lng()}` })
                if(!selectedAccounts.includes(accountId)) {
                    selectedAccounts.push(accountId);
                }
            }
        }
    });

    marker.addListener("dblclick", () => {
        marker.setIcon(null)
    });

    marker.addListener("mouseover", (event) => {
        infoWindow.close();
        infoWindow.setContent(accountName);
        infoWindow.open(marker.getMap(), marker);
    });
    marker.addListener("mouseout", () => {
        infoWindow.close();
    });
}

function parseLocation (place) {
    return place.replace(/\s+/g, '+').replace(/,/g, '%2C');
}
