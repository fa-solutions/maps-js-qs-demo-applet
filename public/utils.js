function getParamsFromLegs(legs) {
    let origin = "";
    let destination = ""
    let wayPoints = [];
    legs.map((waypoint, index) => {
        if(index === 0) {
            origin = parseLocation(waypoint.start_address);
            return waypoint;
        }
        if(index === legs.length - 1) {
            destination = parseLocation(waypoint.end_address);
            if(legs.length > 2) {
                wayPoints.push(parseLocation(waypoint.start_address))
            }
            return waypoint;
        }
        wayPoints.push(parseLocation(waypoint.start_address));
    });

    let params = `origin=${origin}&destination=${destination}`;

    if(wayPoints.length > 0) {
        params = `${params}&waypoints=${wayPoints.join('|')}`
    }
    return params;
}




function getPlaceIdParams(geocodedWaypoints) {
    let originPlaceId = "";
    let destinationPlaceId = ""
    let wayPoints = [];
    geocodedWaypoints.map((waypoint, index) => {
        if(index === 0) {
            originPlaceId = waypoint.place_id;
            return waypoint;
        }
        if(index === geocodedWaypoints.length - 1) {
            destinationPlaceId = waypoint.place_id;
            return waypoint;
        }
        wayPoints.push(waypoint.place_id);
    });

    let params = `origin_place_id=${originPlaceId}&destination_place_id=${destinationPlaceId}`;

    if(wayPoints.length > 0) {
        params = `${params}&waypoint_place_ids=${wayPoints.join('|')}`
    }
    return params;
}


let parseWithLabel = (waypoints) => {
    let waypointsArray = [];
    waypoints.split('|').map((wayp, index) => {
        waypointsArray.push(`markers=color:blue%7Clabel:${index + 1}%7C${parseLocation(wayp)}`);
    })
    return waypointsArray.join('&');
}


let parseTime = (time) => {
    if(time.includes(AM)) {
        return `${time.split(':')[0]}:${time.split(':')[1].substring(0,2)}:00`
    } else {
        return `${parseFloat(time.split(':')[0]) + 12}:${time.split(':')[1].substring(0,2)}:00`
    }
};

let toDataURL = (url) => {
    return fetch(url).then((response) => {
        return response.blob();
    }).then(blob => {
        return URL.createObjectURL(blob);
    });
}

let getLocation = (lonLatStr) => {
    return {
        "lat": parseFloat(lonLatStr.split(',')[0]),
        "lng": parseFloat(lonLatStr.split(',')[1])
    }
}


async function postData(url = '', data = {}) {
    const response = await fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    });
    return response.json();
}


let download = async (url, name='Route') => {
    const a = document.createElement("a");
    a.href = await toDataURL(url);
    a.download = `${name}.png`;
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
}

function updateRecord(id=recordId, entity,fieldValues={}) {
    let updatePayload = {
        entity: entity,
        id: id,
        field_values: fieldValues
    }
    FAClient.updateEntity(updatePayload, (data) => {
        console.log(data);
    })
}

Date.prototype.addHours = function(h) {
    this.setTime(this.getTime() + (h*60*60*1000));
    return this;
}
